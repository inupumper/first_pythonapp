import os

from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_basicauth import BasicAuth

##settings
app = Flask(__name__)
app.config['BASIC_AUTH_FORCE'] = True
app.config['BASIC_AUTH_USERNAME'] = 'jvazquez'
app.config['BASIC_AUTH_PASSWORD'] = 'aburgara'
basic_auth = BasicAuth(app)

app.config.from_object('config.DevelopmentConfig')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String())
    fecha_nacimiento = db.Column(db.String())
    puesto = db.Column(db.String())

    def __init__(self, nombre, fecha_nacimiento, puesto):
        self.nombre = nombre
        self.fecha_nacimiento = fecha_nacimiento
        self.puesto = puesto

    def __repr__(self):
        return '<id {}>'.format(self.id)
    
    def serialize(self):
        return {
            'id': self.id, 
            'nombre': self.nombre,
            'fecha_nacimiento': self.fecha_nacimiento,
            'puesto':self.puesto
        }

db.create_all()

##setings end
@app.route('/personas')
@basic_auth.required
def get_all():
    try:
        personas=User.query.all()
        return  jsonify([e.serialize() for e in personas])
    except Exception as e:
	    return(str(e))

@app.route('/persona/<id_>')
@basic_auth.required
def get_by_id(id_):
    try:
        persona=User.query.filter_by(id=id_).first()
        return jsonify(persona.serialize())
    except Exception as e:
	    return(str(e))


@app.route('/persona', methods=["POST"])
@basic_auth.required
def insert():
    content = request.json
    nombre=content['nombre']
    fecha_nacimiento=content['fecha_nacimiento']
    puesto=content['puesto']
    try:
        persona=User(
            nombre=nombre,
            fecha_nacimiento=fecha_nacimiento,
            puesto=puesto
        )
        db.session.add(persona)
        db.session.commit()
        return "Persona agregada. con el id={}".format(persona.id)
    except Exception as e:
	    return(str(e))

@app.route('/persona/<id_>', methods=["PUT"])
@basic_auth.required
def update(id_):
    try:
        persona=User.query.filter_by(id=id_).first()
        content = request.json
        nombre=content['nombre']
        fecha_nacimiento=content['fecha_nacimiento']
        puesto=content['puesto']
        if nombre != None:
            persona.nombre=nombre
        if fecha_nacimiento != None:
            persona.fecha_nacimiento = fecha_nacimiento
        if puesto != None:
            persona.puesto = puesto
        db.session.commit()
        return "Persona actualizada. con el id={}".format(persona.id)
    except Exception as e:
        return(str(e))



@app.route('/persona/<id_>', methods=["DELETE"])
@basic_auth.required
def delete(id_):
    try:
        persona=User.query.filter_by(id=id_).first()
        db.session.delete(persona)
        db.session.commit()
        return "Persona borrada. con el id={}".format(persona.id)
    except Exception as e:
        return(str(e))



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)