# First_pythonapp

1. Aplicación en Python Rest API con Framework Flask,  para manejar el catálogo de una tabla en la base de datos

   i.      Versión 3 + de Python

   ii.      Base Datos Postgres

   iii.      Nombre Tabla Catalogo: “Users”

   iv.      Campos; “nombre” “fecha nacimiento” “puesto”

   v.      métodos: POST-PUT-GET-DELETE

   vi.      Autenticación Basic Auth con token fijo

## Comenzando 🚀

Clonar el proyecto en tu maquina con git clone https://inupumper@bitbucket.org/inupumper/first_pythonapp.git

Mira **https://pythonapp.syspronet.com/personas** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

1.- Docker & docker-compose

### Instalación 🔧

docker-compose up -d

### Metodos

GET - https://pythonapp.syspronet.com/personas
GET - https://pythonapp.syspronet.com/persona/1
POST - https://pythonapp.syspronet.com/persona
PUT - https://pythonapp.syspronet.com/persona/1
DELETE - https://pythonapp.syspronet.com/persona/1

### Auth token
autenticacion basica = jvazquez:aburgara

### Headers
 "Content-Type": "application/json",